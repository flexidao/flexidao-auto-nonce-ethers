# auto-nonce-ethers

## Overview

AutoNonceWallet Extends Wallet (ethers.js)

Utilizes parity's: `parity_nextNonce` and `parity_pendingTransactions` to get the right nonce.
Catches the next nonce for the new wallet instance and assignes the right nonce to transactions avoiding concurency issues.
Allows to safely send transactions in batches.


## TODOs
- [] make it compatible to work with Nevermind client
- [] bump ethers version to 5.X