const assert = require('assert');
import { Mutex }                                                    from 'await-semaphore';
import { JsonRpcProvider, TransactionRequest, TransactionResponse } from "ethers/providers";

/**
 @param opts {Object}
 @param {Object} opts.provider a ethereum provider
 @param {Function} opts.getPendingTransactions a function that returns an array of txMeta
 whosee status is `submitted`
 @param {Function} opts.getConfirmedTransactions a function that returns an array of txMeta
 whose status is `confirmed`
 @class
 */


export class NonceTracker {
    private getConfirmedTransactions: any;
    private getPendingTransactions: any;
    private provider: JsonRpcProvider;
    private lockMap: any = {};

    // @ts-ignore
    constructor({ provider, getPendingTransactionList, getConfirmedTransactionList }) {
        this.provider = provider;
        this.getPendingTransactions = getPendingTransactionList;
        this.getConfirmedTransactions = getConfirmedTransactionList;
        this.lockMap = {};
    }

    /**
     @returns {Promise<Object>} with the key releaseLock (the gloabl mutex)
     */
    async getGlobalLock() {
        const globalMutex = this._lookupMutex('global');
        // await global mutex free
        const releaseLock = await globalMutex.acquire();
        return { releaseLock }
    }

    /**
     * @typedef NonceDetails
     * @property {number} highestLocallyConfirmed - A hex string of the highest nonce on a confirmed transaction.
     * @property {number} nextNetworkNonce - The next nonce suggested by the eth_getTransactionCount method.
     * @property {number} highestSuggested - The maximum between the other two, the number returned.
     */

    /**
     this will return an object with the `nextNonce` `nonceDetails` of type NonceDetails, and the releaseLock
     Note: releaseLock must be called after adding a signed tx to pending transactions (or discarding).
     @param address {string} the hex string for the address whose nonce we are calculating
     @returns {Promise<NonceDetails>}
     */
    async getNonceLock(address: string) {
        // // await global mutex free
        // await this._globalMutexFree();
        // await lock free, then take lock
        const releaseLock = await this._takeMutex(address);
        try {
            // evaluate multiple nextNonce strategies
            const networkNonceResult = await this._getNetworkNextNonce(address);
            const highestLocallyConfirmed = this._getHighestLocallyConfirmed(address);
            const highestSuggested = Math.max(networkNonceResult, highestLocallyConfirmed);

            const pendingTxs = this.getPendingTransactions(address);
            const localNonceResult = this._getHighestContinuousFrom(pendingTxs, highestSuggested).highest || 0;

            const nextNonce = Math.max(networkNonceResult, localNonceResult);
            assert(
                Number.isInteger(nextNonce),
                `nonce-tracker - nextNonce is not an integer - got: (${typeof nextNonce}) "${nextNonce}"`
            );
            // logger.debug("nonce: %o", nextNonce);
            // return nonce and release cb
            return { nextNonce, releaseLock }
        } catch (err) {
            // release lock if we encounter an error
            releaseLock();
            throw err
        }
    }

    async _globalMutexFree() {
        const globalMutex = this._lookupMutex('global');
        const releaseLock = await globalMutex.acquire();
        releaseLock()
    }

    async _takeMutex(lockId: string) {
        const mutex = this._lookupMutex(lockId);
        return await mutex.acquire()
    }

    _lookupMutex(lockId: string) {
        let mutex = this.lockMap[lockId];
        if (!mutex) {
            mutex = new Mutex();
            this.lockMap[lockId] = mutex
        }
        return mutex
    }


    async _getNetworkNextNonce(address: string) {

        // const networkNextNonce1 = await this.provider.getTransactionCount(address, "pending");
        const networkNextNonce = await this.provider.send("parity_nextNonce", [address]);
        // logger.debug("network next nonce parity: %o, pending: %o", parseInt(networkNextNonce, 16), networkNextNonce1);
        assert(
            Number.isInteger(parseInt(networkNextNonce, 16)),
            `nonce-tracker - baseCount is not an integer - got: (${typeof networkNextNonce}) "${networkNextNonce}"`
        );
        return parseInt(networkNextNonce, 16);
    }

    _getHighestLocallyConfirmed(address: string) {
        const confirmedTransactions = this.getConfirmedTransactions(address);
        const highest = this._getHighestNonce(confirmedTransactions);
        // @ts-ignore
        // logger.debug("highest locally confirmed %o", Number.isInteger(highest) ? highest + 1 : 0);
        // @ts-ignore
        return Number.isInteger(highest) ? highest + 1 : 0
    }

    _getHighestNonce(txList: TransactionResponse[] | undefined) {
        if (typeof txList == "undefined") return;
        const nonces = txList.map((txMeta: TransactionResponse) => {
            const nonce = txMeta.nonce;
            assert(typeof nonce, 'number', 'nonce should be int type');
            return nonce;
        });
        // logger.debug("confirmed nonces \n %o", nonces);
        return Math.max.apply(null, nonces)
    }

    /**
     @typedef {object} highestContinuousFrom
     @property {string} - name the name for how the nonce was calculated based on the data used
     @property {number} - nonce the next suggested nonce
     @property {object} - details the provided starting nonce that was used (for debugging)
     */
    /**
     @param txList {array} - list of txMeta's
     @param startPoint {number} - the highest known locally confirmed nonce
     @returns {highestContinuousFrom}
     */
    _getHighestContinuousFrom(txList: TransactionRequest[] | undefined, startPoint: number) {
        if (typeof txList === "undefined") return { highest: 0, startPoint };
        const nonces = txList.map((txMeta: TransactionRequest) => {
            const nonce = txMeta.nonce;
            assert(typeof nonce, 'number', 'nonce should be a number');
            return nonce
        });
        // logger.debug("pending nonces \n %o", nonces);
        let highest = startPoint;
        while (nonces.includes(highest)) {
            highest++
        }
        // logger.debug("highest continuous nonce %o, startPoint %o", highest, startPoint);
        return { highest, startPoint };
    }
}

export class NonceTrackerSingleton {
    private readonly confirmedTransactions: any;
    private readonly pendingTransactions: any;
    private provider: JsonRpcProvider;
    private mutex: Mutex;

    // @ts-ignore
    constructor({ provider }, pendingTransactionList, confirmedTransactionList) {
        this.provider = provider;
        this.pendingTransactions = pendingTransactionList;
        this.confirmedTransactions = confirmedTransactionList;
        this.mutex = new Mutex();
    }

    async getNonceLock(address: string) {

        const releaseLock = await this._takeMutex();
        try {
            // evaluate multiple nextNonce strategies
            const networkNonceResult = await this._getNetworkNextNonce(address);
            const highestLocallyConfirmed = this._getHighestLocallyConfirmed();
            const highestSuggested = Math.max(networkNonceResult, highestLocallyConfirmed);

            const pendingTxs = this.pendingTransactions;
            const localNonceResult = this._getHighestContinuousFrom(pendingTxs, highestSuggested).highest || 0;

            const nextNonce = Math.max(networkNonceResult, localNonceResult);
            assert(
                Number.isInteger(nextNonce),
                `nonce-tracker - nextNonce is not an integer - got: (${typeof nextNonce}) "${nextNonce}"`
            );
            // logger.debug("nonce: %o", nextNonce);
            // return nonce and release cb
            return { nextNonce, releaseLock }
        } catch (err) {
            // release lock if we encounter an error
            releaseLock();
            throw err
        }
    }

    async _takeMutex() {
        return await this.mutex.acquire()
    }

    async _getNetworkNextNonce(address: string) {

        const networkNextNonce1 = await this.provider.getTransactionCount(address, "pending");
        const networkNextNonce = await this.provider.send("parity_nextNonce", [address]);
        // logger.debug("network next nonce parity: %o, pending: %o", parseInt(networkNextNonce, 16), networkNextNonce1);
        assert(
            Number.isInteger(parseInt(networkNextNonce, 16)),
            `nonce-tracker - baseCount is not an integer - got: (${typeof networkNextNonce}) "${networkNextNonce}"`
        );
        return parseInt(networkNextNonce, 16);
    }

    _getHighestLocallyConfirmed() {
        const highest = this._getHighestNonce(this.confirmedTransactions);
        // @ts-ignore
        // logger.debug("highest locally confirmed %o", Number.isInteger(highest) ? highest + 1 : 0);
        // @ts-ignore
        return Number.isInteger(highest) ? highest + 1 : 0
    }

    _getHighestNonce(txList: TransactionResponse[] | undefined) {
        if (typeof txList == "undefined") return;
        const nonces = txList.map((txMeta: TransactionResponse) => {
            const nonce = txMeta.nonce;
            assert(typeof nonce, 'number', 'nonce should be int type');
            return nonce;
        });
        // logger.debug("confirmed nonces \n %o", nonces);
        return Math.max.apply(null, nonces)
    }

    _getHighestContinuousFrom(txList: TransactionRequest[] | undefined, startPoint: number) {
        if (typeof txList === "undefined") return { highest: 0, startPoint };
        const nonces = txList.map((txMeta: TransactionRequest) => {
            const nonce = txMeta.nonce;
            assert(typeof nonce, 'number', 'nonce should be a number');
            return nonce
        });
        // logger.debug("pending nonces \n %o", nonces);
        let highest = startPoint;
        while (nonces.includes(highest)) {
            highest++
        }
        // logger.debug("highest continuous nonce %o, startPoint %o", highest, startPoint);
        return { highest, startPoint };
    }
}
