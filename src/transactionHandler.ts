import { TransactionReceipt, TransactionRequest, TransactionResponse } from "ethers/providers";
import { isPromise }                                                   from "./utils/isPromise";
import { Bucket }                                                      from "./types";

// pending -> confirmed -> mined

const pendingTransactions: Bucket<TransactionRequest[] | undefined> = {};

const confirmedTransactions: Bucket<TransactionResponse[] | undefined> = {};

export const insertPendingTransaction = (tx: TransactionRequest) => {
    if (typeof tx.from == "undefined" || isPromise(tx.from)) throw new Error(
        "'from:' field is required in th transaction request");
    // @ts-ignore
    if (typeof pendingTransactions[tx.from] == "undefined") {
        // @ts-ignore
        pendingTransactions[tx.from] = [];
    }
    // @ts-ignore
    pendingTransactions[tx.from].push(tx);
};

export const insertConfirmedTransaction = (tx: TransactionResponse) => {
    if (typeof confirmedTransactions[tx.from] == "undefined") {
        confirmedTransactions[tx.from] = [];
    }
    // @ts-ignore
    confirmedTransactions[tx.from].push(tx);
    removePendingTransaction(tx);
};

export const removePendingTransaction = (txRes: TransactionResponse) => {
    if (typeof pendingTransactions[txRes.from] != "undefined") {
        // @ts-ignore
        const index = pendingTransactions[txRes.from].findIndex((txReq: TransactionRequest) => (txReq.from
                                                                                                == txRes.from
                                                                                                && txReq.nonce
                                                                                                == txRes.nonce));
        // @ts-ignore
        if (index != -1) pendingTransactions[txRes.from].splice(index, 1);
        else console.error("Transaction not found in pending transactions")
    } else {
        console.error("pendingTransactions for address %s is undefined", txRes.from)
    }
};

export const removeConfirmedTransaction = (txRec: TransactionReceipt) => {
    // @ts-ignore
    const index = confirmedTransactions[txRec.from].findIndex((txRes: TransactionResponse) => (txRes.hash
                                                                                               == txRec.transactionHash));
    if (index != -1) {
        // @ts-ignore
        confirmedTransactions[txRec.from].splice(index, 1);
    } else console.error("Transaction not found in confirmed transactions")
};


export const getPendingTransactionList = (address: string): TransactionRequest[] | undefined => {
    return pendingTransactions[address];
};

export const getConfirmedTransactionList = (address: string): TransactionResponse[] | undefined => {
    return confirmedTransactions[address];
};