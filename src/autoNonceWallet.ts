import { Wallet } from "ethers";
import { Provider, TransactionReceipt, TransactionRequest, TransactionResponse } from "ethers/providers";
import { Arrayish, SigningKey } from "ethers/utils";
import { HDNode } from "ethers/utils/hdnode";
import { isPromise } from "./utils/isPromise";
import { NonceTrackerSingleton } from "./nonceTracker";

export class AutoNonceWallet extends Wallet {

    private nonceTracker: NonceTrackerSingleton;
    private pendingTransactionList: TransactionRequest[];
    private confirmedTransactionList: TransactionResponse[];

    constructor(privateKey: SigningKey | HDNode | Arrayish, provider: Provider) {
        super(privateKey, provider);
        this.pendingTransactionList = [];
        this.confirmedTransactionList = [];
        this.nonceTracker = new NonceTrackerSingleton(
            { provider },
            this.pendingTransactionList,
            this.confirmedTransactionList
        )
    }

    async sendTransaction(transaction: TransactionRequest): Promise<TransactionResponse> {

        const nonce = await this.getNonce();
        transaction.nonce = nonce.nextNonce;

        this._insertPendingTransaction(Object.assign({ from: super.address }, transaction));
        const tx = super.sendTransaction(transaction);

        nonce.releaseLock();
        return tx;
    };

    getNonce = () => this.nonceTracker.getNonceLock(super.address);

    _insertPendingTransaction = (tx: TransactionRequest) => {
        if (typeof tx.from == "undefined" || isPromise(tx.from)) throw new Error(
            "'from:' field is required in th transaction request");
        this.pendingTransactionList.push(tx);
    };

    insertConfirmedTransaction(tx: TransactionResponse) {
        this.confirmedTransactionList.push(tx);
        this._removePendingTransaction(tx);
    };

    _removePendingTransaction(txRes: TransactionResponse) {
        const index = this.pendingTransactionList.findIndex((txReq: TransactionRequest) => (txReq.from
                                                                                            == txRes.from
                                                                                            && txReq.nonce
                                                                                            == txRes.nonce));
        if (index != -1) this.pendingTransactionList.splice(index, 1);
        else console.error("Transaction not found in pending transactions")
    };

    removeConfirmedTransaction(txRec: TransactionReceipt) {
        const index = this.confirmedTransactionList.findIndex((txRes: TransactionResponse) => (txRes.hash
                                                                                               == txRec.transactionHash));
        if (index != -1) {
            this.confirmedTransactionList.splice(index, 1);
        } else console.error("Transaction not found in confirmed transactions")
    };

    getPendingTransactionList() {
        return this.pendingTransactionList
    }

    getConfirmedTransactionList() {
        return this.confirmedTransactionList
    }

}

// pending -> confirmed -> mined


// https://ethereum.stackexchange.com/questions/39790/concurrency-patterns-for-account-nonce
// https://ethereum.stackexchange.com/questions/2808/what-happens-when-a-transaction-nonce-is-too-high
