import { TransactionReceipt, TransactionRequest } from "ethers/providers";
import { AutoNonceWallet }                        from "./autoNonceWallet";
import { TransactionOverrides }                   from "./types";

export class BatchSendTransaction {

    private wallet: AutoNonceWallet;

    constructor(wallet: AutoNonceWallet) {
        this.wallet = wallet;
    }

    async addTransaction(transaction: TransactionRequest, overrides?: TransactionOverrides) {
        const sendPromise = await this.wallet.sendTransaction(Object.assign(transaction, overrides));
        this.wallet.insertConfirmedTransaction(sendPromise);
        return this.wallet.getConfirmedTransactionList;
    };

    getPendingTransactions() {
        return this.wallet.getPendingTransactionList()
    };

    getConfirmedTransactionList() {
        return this.wallet.getConfirmedTransactionList()
    };

    async awaitForAll() {
        const transferPromisses = this.wallet.getConfirmedTransactionList().map((transfer) => transfer.wait());
        const transactionReceiptList: TransactionReceipt[] = await Promise.all(transferPromisses);
        transactionReceiptList.forEach((txReciept: TransactionReceipt) => {
            this.wallet.removeConfirmedTransaction(txReciept);
        });
        return transactionReceiptList;
    };

}

// pending -> confirmed -> mined

// https://ethereum.stackexchange.com/questions/39790/concurrency-patterns-for-account-nonce
// https://ethereum.stackexchange.com/questions/2808/what-happens-when-a-transaction-nonce-is-too-high
