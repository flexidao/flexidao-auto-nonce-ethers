import { dest, src, task } from "gulp";
import * as ts             from "gulp-typescript";

const tsProject = ts.createProject('tsconfig.json');

task('ts:build', () => {
    return tsProject.src()
    .pipe(tsProject())
    // this is to tolerate compilation errors and copy the js files anyway
    // .on('error', () => { /* Ignore compiler errors */ })
    .pipe(dest('dist'))
    .pipe(src(['./src/**/*', '!./**/*.ts']))
    .pipe(dest('dist'));
});

